﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FunnyWeb.Startup))]
namespace FunnyWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
